package TestRunners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src\\main\\java\\Features\\MenuManagment.feature",
        glue = {"StepDefinition"},
        tags = "@SmokeTest or @RegularTest or @NegativeTest or @Panic",
//        tags = "@Panic",
//        tags = "not @RegularTest",
        plugin = {"pretty"}
)

public class MenuManagmentTest {


}
