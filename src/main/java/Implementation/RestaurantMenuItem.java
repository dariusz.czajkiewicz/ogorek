package Implementation;

public class RestaurantMenuItem {
    String Name;
    String Description;
    Integer price;

    public RestaurantMenuItem(String name, String description, Integer price) {
        Name = name;
        Description = description;
        this.price = price;
    }

    public String getMenuItemName(){
        return Name;
    }
}
