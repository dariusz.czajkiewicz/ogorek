package StepDefinition;

import Implementation.RestaurantMenuItem;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import Implementation.RestaurantMenu;
import io.cucumber.java.lu.an;
import static org.junit.Assert.assertEquals;

public class MenuManagmentSteps {
    RestaurantMenuItem NewMenuItem;
    RestaurantMenu LocationMenu =  new RestaurantMenu();
    String ErrorMessage;

    @Given("I have menu item with name {string} and price {int}")
    //@Given("I have menu item with name \"([^\"]+)\" and price ([0-9]+)")
    public void i_have_menu_item_with_name_and_price(String NewMenuItemName, Integer price) {
        //RestaurantMenuItem NewMenuItem;
        NewMenuItem = new RestaurantMenuItem(NewMenuItemName, NewMenuItemName, price);
        System.out.println("Step 1");
    }

    @When("I add that menu item")
    public void i_add_that_menu_item() {
        try {
            LocationMenu.AddMenuItem(NewMenuItem);
        }
        catch (IllegalArgumentException ex) {
            ErrorMessage = ex.getMessage();
        }
        System.out.println("Step 2");
    }

    @Then("Menu Item with name {string} should be added")
    public void menu_item_with_name_should_be_added(String string) {
        boolean Exists = LocationMenu.DoesItemExists(NewMenuItem);
        System.out.println("Step 3: " + Exists);
    }

    @Then("I should see an error message with value {string}")
        public void is_should_see_an_message_with_value(String string){
        assertEquals(string,ErrorMessage);
    }

    /*@Then("I should see an error message with value")
        public void iShouldSeeAnErrorMessageWithValue() {
    }*/
}
