Feature: Menu Managment

  Background: Add third menu item
    Given I have menu item with name "Background Sandwich" and price 15
    When I add that menu item
    Then Menu Item with name "Background Sandwich" should be added

  @SmokeTest
  Scenario: Add a menu item
    Given I have menu item with name "Cucumber Sandwich Second" and price 20
    When I add that menu item
    Then Menu Item with name "Cucumber Sandwich" should be added

  @RegularTest
  Scenario: Add another menu item
    Given I have menu item with name "Cucumber Sandwich" and price 10
    When I add that menu item
    Then Menu Item with name "Cucumber Sandwich" should be added

  @NightlyBuildTest @RegularTest
  Scenario: Add third menu item
    Given I have menu item with name "Cucumber Sandwich" and price 15
    When I add that menu item
    Then Menu Item with name "Cucumber Sandwich" should be added

  @NegativeTest
  Scenario: NegativeTest menu item
    Given I have menu item with name "Negative Sandwich" and price 15
    When I add that menu item
    When I add that menu item
    Then I should see an error message with value "Duplicate Item: Negative Sandwich"

  @Panic
  Scenario: Panic menu item
    Given I have menu item with name "Panic Sandwich" and price 15
    When I add that menu item
    And  I add that menu item
    Then I should see an error message with value "Duplicate Item: Panic Sandwich"